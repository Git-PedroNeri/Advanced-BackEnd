## Orquestração com Kubernetes
### Minhas anotações

### Comandos kubernetes

#### ```kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml```
#### ```kubectl proxy```
#### Acessa o dashboard do kubernetes pela url:
#### ```http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/```

### Credenciais
#### Dentro do diretorio dos arquivos yaml criamos usuario e roles para esse usuario
#### ```kubectl apply -f .\dashboard-adminuser.yaml```
#### ```kubectl apply -f .\clusterRoleBinding.yaml```
#### Token para acessar dashboard
#### ```kubectl -n kubernetes-dashboard create token admin-user```

#### Remove permissao do usuario
#### ```kubectl -n kubernetes-dashboard delete clusterrolebinding admin-user```

#### Remover usuario
#### ```kubectl -n kubernetes-dashboard delete admin-user```


## Criando inúmeras aplicações

#### Build na imagem docker a partir do dockerfile na aplicação gitlab-ci-cd-hello-world
#### ```docker build . -t ng-docker-ks8:1.0```

#### Apply na aplicação de dentro do diretorio do angular projeto : gitlab-ci-cd-hello-world
-Isso faz levantar 5 pods no kubernetes como servico
#### ```kubectl apply -f ng-deployment.yaml```

#### Aplicação de loadbalancer para acesso a aplicação;
```kubectl expose deployment angular-deployment --type=LoadBalancer --port=8080 --target-port=80```

## Rancher

#### ```docker run -d --name rancher-server --restart=unless-stopped -p 80:80 -p 443:443 --privileged rancher/rancher:latest --no-cacerts```

#### getPassword
#### ```Dentro do logs da imagem Bootstrap password```

#### Mapeamento local do rancher -> ngrok
#### ```ngrok http https://localhost/```
- Agora temos uma url disponivel na internet ridando local
- Depois cria um usuario dentro do rancher para acesso exerno.

## Integração GitLab Kubernetes

Agente de integração para integrar gitlakb no kubernetes




## Aula

Após a habilitação do Kubernetes e de aguardar alguns poucos minutos, será possível enxergar na lista de contêineres do Docker Desktop os principais PODs do Kubernetes já em execução local. Para realizarmos alguns testes preliminares, podemos seguir com a instalação do dashboard do cluster seguindo os seguintes passos:

Execute o comando para instalar o contêiner contendo a implementação do dashboard:
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
Certifique-se que a porta 8001 está liberada (se o primeiro comando lsof retornar vazio, vá direto para a etapa 3):
$ sudo lsof -i :8001
$ kill -9 <PID dos Processos usando portas 8001>
Execute o dashboard:
$ kubectl proxy
Antes de visitarmos o dashboard, precisamos de um usuário devidamente credenciado para o acesso. Navegue até o diretório raíz do projeto desta disciplina em https://github.com/FaculdadeDescomplica/Advanced-BackEnd/. Lá você encontrará dois arquivos de configuração Kubernetes, sendo eles o "dashboard-adminuser.yaml" e o "clusterRoleBinding.yaml". Faça a criação do usuário e credencie-o para acessar o cluster:
$ kubectl apply -f dashboard-adminuser.yaml
$ kubectl apply -f clusterRoleBinding.yaml
Com o usuário criado e credenciado, criei um token de acesso:
$ kubectl -n kubernetes-dashboard create token admin-user
Anote o token fornecido na etapa 5, e acesse a URL da plataforma em: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/. Insira o token e faça o logon. Você deverá enxergar uma tela similar a que é mostrada pela Figura 3.
Figura 3: dashboard do Kubernetes por onde podemos acompanhar métricas. Fonte: catálogo do professor autor.
Figura 3: dashboard do Kubernetes por onde podemos acompanhar métricas. Fonte: catálogo do professor autor.

Agora com o Kubernetes instalado e com o dashboard operante, vamos testar alguns comandos. Em um terminal/prompt, execute as seguintes tarefas:

Verifique a versão do seu Kubernetes:
$ kubectl version
Verifique o contexto do seu Kubernetes:
$ kubectl config current-context
Obtenha informações do cluster:
$ kubectl cluster-info
Liste os servidores nós em execução:
$ kubectl get nodes
Por fim, vamos agora criar uma aplicação conteinerizada para nosso cluster. O setup que este laboratório foi construído contempla as seguintes ferramentas:

Sistema Operacional OSX 11.7.1 BigSur;
Docker Desktop v4.15.0;
xCode com a versão mais recente:
$ sudo rm -rf /Library/Developer/CommandLineTools
$ xcode-select --install
NodeJS v16.13.0
NPM v8.1.0
Angular v15.0.1
Nginx v1.23.3
A configuração exata deste conjunto de ferramentas não é obrigatória, e porventura sua implementação poderá ser criada sem problemas mesmo com algumas versões diferentes. É interessante salientar que instabilidades de versionamento e possíveis incompatibilidades podem vir a ocorrer com outro setup. Para executarmos nossa aplicação, procedamos com os seguintes passos:

Com o Docker Desktop aberto, navegue via prompt até a raíz do projeto:
https://github.com/FaculdadeDescomplica/Advanced-BackEnd/gitlab-ci-cd-hello-world;
Faça a atualização e download das dependências do Angular:
$ npm install
Execute o localhost da aplicação Angular para verificação (acessível em http://localhost:4200)
$ ng serve -o
Crie o pacote para deployment:
$ ng build
Construa uma imagem do Docker para ser executada em contêiner:
$ docker build -t ng-docker-k8s:1.0 .
Suba a imagem no seu cluster Kubernetes, usando o arquivo ng-deployment como ponto de configuração:
$ kubectl apply -f ng-deployment.yaml
Leia o arquivo ng-deployment.yaml. Entenda o que cada linha traz e define. Percebeu que existem configurações, inclusive, do número de PODs? Estamos subindo uma aplicação tolerante a falhas!
Quando temos uma aplicação com vários PODs, quem as acessa de fora irá usar qual POD? Esta pergunta é respondida pelo LoadBalancer, um serviço que escolhe de forma criteriosa e equilibrada qual POD será disponibilizado a um cliente solicitante. Vamos criar o LoadBalancer:
$ kubectl expose deployment angular-deployment --type=LoadBalancer --port=8080 --target-port=80 
Visite a página http://localhost:8080 e confira seu serviço Angular operante. A Figura 4 mostra o que você deverá estar enxergando:
Figura 4: aplicação Angular em http://localhost:8080 após subir em cluster Kubernetes. Fonte: catálogo do professor autor.
Figura 4: aplicação Angular em http://localhost:8080 após subir em cluster Kubernetes. Fonte: catálogo do professor autor.

Depois de verificar sua aplicação Angular, derrube o serviço e o deployment (não deixe recursos sendo utilizados sem propósito):
$ kubectl delete -n default deployment angular-deployment 
$ kubectl delete -n default service angular-service


Conteúdo bônus

Tópicos avançados

O principal comando para interfacear com o cluster Kubernetes é o "kubectl". Fazer uma leitura das documentações do mesmo, bem como entender como ele funciona e quais os seus principais parâmetros é uma ótima idéia para alavancar seu conhecimento sobre Kubernetes. Apenas lembre-se que este comando só estará acessível se o Kubernetes estiver instalado na máquina em que estiver tentando chamá-lo.

Você pode ainda ter muito interesse no comando "kubectl delete", que faz a exclusão, apagamento, remoção, ou cancelamento de recursos do cluster. A forma mais comum de executá-lo é da seguinte maneira:

$ kubectl -n <target> delete <tipo_recurso> <nome_recurso>

Para cancelar o credenciamento do usuário admin de nosso dashboard (um dos nossos exemplos práticos desta aula) você pode proceder com a seguinte linha:

$ kubectl -n kubernetes-dashboard delete serviceaccount admin-user

Ainda, algo interessante de ser mencionado é sobre o local em que o Kubernetes do Docker Desktop armazena credenciais e configurações. Em máquinas OSX, o local fica em "~/.kube/config", e o conhecimento desta localidade é interessante caso você identifique que o seu ambiente encontra-se instável ou operando com processos/contêineres zumbis (que não podem ser derrubados).



Caro estudante, você consegue acessar os códigos utilizados na disciplina no link a seguir:

https://github.com/FaculdadeDescomplica/Advanced-BackEnd

Referência Bibliográfica

Página do Projeto Kubernetes. Orquestração de Contêineres Prontos para Produção, 2023. Disponível em: https://kubernetes.io/pt-br/. Acesso em: 24/01/2023.